import axios from '@/utils/api.request'
import Cookies from 'js-cookie'

//岗位薪资top10
const get_JobTable_SalaryTop10 = params => {
    return axios.request({
        url: '/get_JobTable_Popularposts',
        method: 'get',
        params: params
    })
}

//求职者各个文化程度的数量
const get_JobTable_Education_JobSeeker = params => {
    return axios.request({
        url: '/get_JobTable_Education_JobSeeker',
        method: 'get',
        params: params
    })
}

//今日入场人数
const get_JobTable_UserCount = params => {
    return axios.request({
        url: '/get_JobTable_UserCount',
        method: 'get',
        params: params
    })
}

//发布岗位数
const get_JobTable_PostsNumber = params => {
    return axios.request({
        url: '/get_JobTable_PostsNumber',
        method: 'get',
        params: params
    })
}

//职位分布地区和数量
const get_JobTable_JobDistribution = params => {
    return axios.request({
        url: '/get_JobTable_JobDistribution',
        method: 'get',
        params: params
    })
}

//岗位供给top10
const get_JobTable_JobSupplyTop10 = params => {
    return axios.request({
        url: '/get_JobTable_JobSupplyTop10',
        method: 'get',
        params: params
    })
}

//岗位缺口top10
const get_JobTable_TalentGapTop10 = params => {
    return axios.request({
        url: '/get_JobTable_TalentGapTop10',
        method: 'get',
        params: params
    })
}

//企业发布岗位数top10
const get_JobTable_CompanyPostsNumberTop10 = params => {
    return axios.request({
        url: '/get_JobTable_CompanyPostsNumberTop10',
        method: 'get',
        params: params
    })
}

//行业分布占比
const get_JobTable_ProportionIndustry = params => {
    return axios.request({
        url: '/get_JobTable_ProportionIndustry',
        method: 'get',
        params: params
    })
}

//行业分布占比
const get_TalentIn_supplyanddemand = params => {
    return axios.request({
        url: '/get_TalentIn_supplyanddemand',
        method: 'get',
        params: params
    })
}

export {
    get_JobTable_SalaryTop10,
    get_JobTable_Education_JobSeeker,
    get_JobTable_UserCount,
    get_JobTable_PostsNumber,
    get_JobTable_JobDistribution,
    get_JobTable_JobSupplyTop10,
    get_JobTable_TalentGapTop10,
    get_JobTable_CompanyPostsNumberTop10,
    get_JobTable_ProportionIndustry,
    get_TalentIn_supplyanddemand
}