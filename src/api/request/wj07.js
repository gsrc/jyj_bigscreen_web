import axios from '@/utils/api.request'


//企业新增入驻
const getCompanyIn = params => {
    return axios.request({
        url: '/enterpriseSettleIn3',
        method: 'get',
        params: params
    })
};
//人企互动概览
const talentEnterpriseInteractive = params => {
    return axios.request({
        url: '/talentEnterpriseInteractive',
        method: 'get',
        params: params
    })
};
//企业镇街多维化信息
const enterpriseCenus = params => {
    return axios.request({
        url: '/enterpriseCenus3',
        method: 'get',
        params: params
    })
};
//企业入驻
const enterpriseSettleInTotal = params => {
    return axios.request({
        url: '/enterpriseSettleInTotal',
        method: 'get',
        params: params
    })
};
//发布岗位企业数
const numberOfPosts = params => {
    return axios.request({
        url: '/numberOfPosts3',
        method: 'get',
        params: params
    })
};

//企业情况占比
const informatization = params => {
    return axios.request({
        url: '/informatization3',
        method: 'get',
        params: params
    })
};
//企业镇街
const streetMultidimensional = params => {
    return axios.request({
        url: '/streetMultidimensional3',
        method: 'get',
        params: params
    })
};


export {
    getCompanyIn,
    talentEnterpriseInteractive,
    enterpriseCenus,
    enterpriseSettleInTotal,
	numberOfPosts,
    informatization,
    streetMultidimensional
}