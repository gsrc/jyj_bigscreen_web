import axios from '@/utils/api.request'
import Cookies from 'js-cookie'

//热点关注
const getPolicySearchKeywords = params => {
    return axios.request({
        url: '/getPolicySearchKeywords',
        method: 'get',
        params: params
    })
}

//文章
const getPolicyNewPolicyTop5 = params => {
    return axios.request({
        url: '/getPolicyNewPolicyTop5',
        method: 'get',
        params: params
    })
}

//活跃用户
const readingUs = params => {
    return axios.request({
        url: '/readingUs',
        method: 'get',
        params: params
    })
}

//最新政策
const getPolicyNewPolicy = params => {
    return axios.request({
        url: '/getPolicyNewPolicy',
        method: 'get',
        params: params  
    })
}

//标签文章类型占比
const getPolicyLabels = params => {
    return axios.request({
        url: '/getPolicyLabels',
        method: 'get',
        params: params
    })
}

//热点关注
const getPolicyHotSpot = params => {
    return axios.request({
        url: '/getPolicyHotSpot',
        method: 'get',
        params: params
    })
}

//出台热门政策TOP10
const getPolicyHotArticles = params => {
    return axios.request({
        url: '/getPolicyHotArticles',
        method: 'get',
        params: params
    })
}
//活动关注主体占比
const eventFocus = params => {
    return axios.request({
        url: '/eventFocus',
        method: 'get',
        params: params
    })
}
//活动信息排名TOP10
const getPolicyHotArticlesOther = params => {
    return axios.request({
        url: '/getPolicyHotArticles?isRCZX=true',
        method: 'get',
        params: params
    })
}
//阅读时间点
const getReadingTime = params => {
    return axios.request({
        url: '/getReadingTime',
        method: 'get',
        params: params
    })
}

//政策咨询发布量(累计发布量) year
const getPolicyPolicyAdvisoryReleases = params => {
    return axios.request({
        url: '/getPolicyPolicyAdvisoryReleases',
        method: 'get',
        params: params
    })
}

//政策咨询发布量(当月发布量)
const getPolicyPolicyAdvisoryReleasesByMonth = params => {
    return axios.request({
        url: '/getPolicyPolicyAdvisoryReleasesByMonth',
        method: 'get',
        params: params
    })
}

//新闻转发量
const getPolicyNewsShare = params => {
    return axios.request({
        url: '/getPolicyNewsShare',
        method: 'get',
        params: params
    })
}

//新闻发布量
const getPolicyNewsRelease = params => {
    return axios.request({
        url: '/getPolicyNewsRelease',
        method: 'get',
        params: params
    })
}

//新闻阅读量
const getPolicyNewsReading = params => {
    return axios.request({
        url: '/getPolicyNewsReading',
        method: 'get',
        params: params
    })
}

//各行业政策占比
const getRegionalPolicy = params => {
    return axios.request({
        url: '/getRegionalPolicy',
        method: 'get',
        params: params
    })
}
//落实政策资金占比
const getFunds = params => {
    return axios.request({
        url: '/getFunds',
        method: 'get',
        params: params
    })
}

export {
  getPolicySearchKeywords,
  getPolicyNewPolicyTop5,
  readingUs,
  getPolicyNewPolicy,
  getPolicyLabels,
  getPolicyHotSpot,
  getPolicyHotArticles,
  getPolicyHotArticlesOther,
  eventFocus,
  getFunds,
  getReadingTime,
  getPolicyPolicyAdvisoryReleases,
  getPolicyPolicyAdvisoryReleasesByMonth,
  getPolicyNewsShare,
  getPolicyNewsRelease,
  getPolicyNewsReading,
  getRegionalPolicy
}