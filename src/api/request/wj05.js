import axios from '@/utils/api.request'
import Cookies from 'js-cookie'

//毕业人数排名
const getNumberOfGraduates = params => {
    return axios.request({
        url: '/numberOfGraduates',
        method: 'get',
        params: params
    })
}

//毕业生性别人数分布
const getSexOfGraduates = params => {
    return axios.request({
        url: '/sexOfGraduates',
        method: 'get',
        params: params
    })
}

//文化程度占比
const getProportionOfEducation = params => {
    return axios.request({
        url: '/proportionOfEducation',
        method: 'get',
        params: params
    })
}

//毕业生总体流向
const getFlowDirection = params => {
    return axios.request({
        url: '/flowDirection',
        method: 'get',
        params: params
    })
}

//就业流向地区分布占比
const getEmploymentArea = params => {
    return axios.request({
        url: '/employmentArea',
        method: 'get',
        params: params
    })
}

//近三年不同学历毕业人数
const getNumberOfGraduatesByEducation = params => {
    return axios.request({
        url: '/NumberOfGraduatesByEducation',
        method: 'get',
        params: params
    })
}


//专业毕业生排名
const getRankingOfProfessionalGraduates = params => {
    return axios.request({
        url: '/rankingOfProfessionalGraduates',
        method: 'get',
        params: params
    })
}


//热门词云
const getHotWordCloud = params => {
    return axios.request({
        url: '/hotWordCloud',
        method: 'get',
        params: params
    })
}

//应届毕业生人数占比
const getAllSchoolNum = params => {
    return axios.request({
        url: '/getAllSchoolNum',
        method: 'get',
        params: params
    })
}

export {
    getNumberOfGraduates,
    getSexOfGraduates,
    getProportionOfEducation,
    getFlowDirection,
    getEmploymentArea,
    getNumberOfGraduatesByEducation,
    getRankingOfProfessionalGraduates,
    getHotWordCloud,
	getAllSchoolNum
}