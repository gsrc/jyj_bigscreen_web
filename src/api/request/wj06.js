import axios from '@/utils/api.request'


//岗位推荐TOP10
const getJobRecommendationTop = params => {
    return axios.request({
        url: '/recommendedPostsTop',
        method: 'get',
        params: params
    })
}

//人才推荐TOP10
const getTalentRecommendationTop = params => {
    return axios.request({
        url: '/recommendedTalentTop',
        method: 'get',
        params: params
    })
}

//推荐岗位
const getJobRecommendation = params => {
    return axios.request({
        url: '/recommendedPosts',
        method: 'get',
        params: params
    })
}

//推荐人才
const getTalentRecommendation = params => {
    return axios.request({
        url: '/recommendedTalent',
        method: 'get',
        params: params
    })
}

//中间数据
const getMiddleData = params => {
    return axios.request({
        url: '/intermediateData',
        method: 'get',
        params: params
    })
}

//行为热点H
const getBehavioralHotspots = params => {
    return axios.request({
        url: '/userHotspot',
        method: 'get',
        params: params
    })
}

export {
    getJobRecommendationTop,
    getTalentRecommendationTop,
    getJobRecommendation,
    getTalentRecommendation,
    getMiddleData,
    getBehavioralHotspots
}