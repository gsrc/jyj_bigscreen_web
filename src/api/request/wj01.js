import axios from '@/utils/api.request'
import Cookies from 'js-cookie'

//热门岗位招聘
const get_TalentMonitor_PopularPosts = params => {
    return axios.request({
        url: '/get_TalentMonitor_PopularPosts',
        method: 'get',
        params: params
    })
}

//期望薪资统计
const get_TalentMonitor_ExpectedSalary = params => {
    return axios.request({
        url: '/get_TalentMonitor_ExpectedSalary',
        method: 'get',
        params: params
    })
}

//签到时间分布
const get_TalentMonitor_CheckInTime = params => {
    return axios.request({
        url: '/get_TalentMonitor_CheckInTime',
        method: 'get',
        params: params
    })
}

//新增人才来源
const get_TalentMonitor_TalentSource = params => {
    return axios.request({
        url: '/get_TalentMonitor_TalentSource',
        method: 'get',
        params: params
    })
}

//求职者环比增长
const get_TalentMonitor_HuanBiIncrease = params => {
    return axios.request({
        url: '/get_TalentMonitor_HuanBiIncrease',
        method: 'get',
        params: params
    })
}

//求职者同比增长
const get_TalentMonitor_TongBiIncrease = params => {
    return axios.request({
        url: '/get_TalentMonitor_TongBiIncrease',
        method: 'get',
        params: params
    })
}

//最新招聘企业
const get_TalentMonitor_LatestRecruitingCompanies = params => {
    return axios.request({
        url: '/get_TalentMonitor_LatestRecruitingCompanies',
        method: 'get',
        params: params
    })
}

//入场企业数
const get_TalentMonitor_CompaniesInNumber = params => {
    return axios.request({
        url: '/get_TalentMonitor_CompaniesInNumber',
        method: 'get',
        params: params
    })
}

//发布岗位数
const get_TalentMonitor_RecruitmentNumber = params => {
    return axios.request({
        url: '/get_TalentMonitor_RecruitmentNumber',
        method: 'get',
        params: params
    })
}

//入场求职者人数
const get_TalentMonitor_JobSeekersNumber = params => {
    return axios.request({
        url: '/get_TalentMonitor_JobSeekersNumber',
        method: 'get',
        params: params
    })
}

//男女职员
const get_TalentMonitor_TotalPeople = params => {
    return axios.request({
        url: '/get_TalentMonitor_TotalPeople',
        method: 'get',
        params: params
    })
}

//年龄区间
const get_TalentMonitor_Age_JobSeeker = params => {
    return axios.request({
        url: '/get_TalentMonitor_Age_JobSeeker',
        method: 'get',
        params: params
    })
}

//学历结构
const get_TalentMonitor_Education_JobSeeker = params => {
        return axios.request({
            url: '/get_TalentMonitor_Education_JobSeeker',
            method: 'get',
            params: params
        })
    }
    //求职者街镇分布
const get_TalentMonitor_talentCenus = params => {
    return axios.request({
        url: '/get_TalentMonitor_talentCenus',
        method: 'get',
        params: params
    })
}
 //求职者总数
const getTotalStaff = params => {
    return axios.request({
        url: '/getTotalStaff',
        method: 'get',
        params: params
    })
}
export {
    get_TalentMonitor_PopularPosts,
    get_TalentMonitor_talentCenus,
	getTotalStaff,
    get_TalentMonitor_ExpectedSalary,
    get_TalentMonitor_CheckInTime,
    get_TalentMonitor_TalentSource,
    get_TalentMonitor_HuanBiIncrease,
    get_TalentMonitor_TongBiIncrease,
    get_TalentMonitor_LatestRecruitingCompanies,
    get_TalentMonitor_CompaniesInNumber,
    get_TalentMonitor_RecruitmentNumber,
    get_TalentMonitor_JobSeekersNumber,
    get_TalentMonitor_TotalPeople,
    get_TalentMonitor_Age_JobSeeker,
    get_TalentMonitor_Education_JobSeeker
}