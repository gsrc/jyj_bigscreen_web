import axios from '@/utils/api.request'
import Cookies from 'js-cookie'

//温江各地区各学历求职者数量
const get_TalentIn_EducationalDistribution = params => {
    return axios.request({
        url: '/get_TalentIn_EducationalDistribution',
        method: 'get',
        params: params
    })
}

//温江各地区各专业求职者数量
const get_TalentIn_ProfessionalDistribution = params => {
    return axios.request({
        url: '/get_TalentIn_ProfessionalDistribution',
        method: 'get',
        params: params
    })
}

//外部人才占比
const get_TalentIn_ExternalTalentProportion = params => {
    return axios.request({
        url: '/get_TalentIn_ExternalTalentProportion',
        method: 'get',
        params: params
    })
}

//人才来源分布情况
const get_TalentIn_TalentSourceDistribution = params => {
    return axios.request({
        url: '/get_TalentIn_TalentSourceDistribution',
        method: 'get',
        params: params
    })
}

//镇街数据
const get_ZJ_data = params => {
    return axios.request({
        url: '/get_ZJ_data',
        method: 'get',
        params: params
    })
}

//医药引进人才
const get_TalentIn_YiYaoTalent = params => {
    return axios.request({
        url: '/get_TalentIn_YiYaoTalent',
        method: 'get',
        params: params
    })
}

//医疗引进人才
const get_TalentIn_YiLiaoTalent = params => {
    return axios.request({
        url: '/get_TalentIn_YiLiaoTalent',
        method: 'get',
        params: params
    })
}

//医学引进人才
const get_TalentIn_YiXueTalent = params => {
    return axios.request({
        url: '/get_TalentIn_YiXueTalent',
        method: 'get',
        params: params
    })
}

//外部人才数
const get_TalentIn_ExternalTalent = () => {
    return axios.request({
        url: '/get_TalentIn_ExternalTalent',
        method: 'get'
    })
}

//高校人才数
const get_TalentIn_SchoolTalent = () => {
    return axios.request({
        url: '/get_TalentIn_SchoolTalent',
        method: 'get'
    })
}

//在职工人数
const get_TalentIn_CurrentEmployee = () => {
    return axios.request({
        url: '/get_TalentIn_CurrentEmployee',
        method: 'get'
    })
}

//三医人才引进占比
const getTalentIntroduction = () => {
    return axios.request({
        url: '/talentIntroduction',
        method: 'get'
    })
}

//三医人才需求占比(三医比非三医)
const get_TalentIn_sanyiTalentDemand_all = (params) => {
    return axios.request({
        url: '/get_TalentIn_sanyiTalentDemand_all',
        method: 'get',
        params: params
    })
}


//三医人才供应占比(三医比非三医)
const get_TalentIn_sanyiTalentSupply_all = (params) => {
    return axios.request({
        url: '/get_TalentIn_sanyiTalentSupply_all',
        method: 'get',
        params: params
    })
}

export {
    get_TalentIn_EducationalDistribution,
    get_TalentIn_ProfessionalDistribution,
    get_TalentIn_ExternalTalentProportion,
    get_TalentIn_TalentSourceDistribution,
	get_ZJ_data,
    get_TalentIn_YiYaoTalent,
    get_TalentIn_YiLiaoTalent,
    get_TalentIn_YiXueTalent,
    get_TalentIn_ExternalTalent,
    get_TalentIn_SchoolTalent,
    get_TalentIn_CurrentEmployee,
    getTalentIntroduction,
    get_TalentIn_sanyiTalentDemand_all,
    get_TalentIn_sanyiTalentSupply_all,
}