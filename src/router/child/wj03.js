
import wj03 from '@/pages/wj03/index'

import head from '@/pages/wj03/header/header'

import leftCol_01 from '@/pages/wj03/left-col/col-01'
import leftCol_02 from '@/pages/wj03/left-col/col-02'
import leftCol_03 from '@/pages/wj03/left-col/col-03'
import midCol_01 from '@/pages/wj03/mid-col/col-01'
import midCol_02 from '@/pages/wj03/mid-col/col-02'
import rightCol_01 from '@/pages/wj03/right-col/col-01'
import rightCol_02 from '@/pages/wj03/right-col/col-02'
import rightCol_03 from '@/pages/wj03/right-col/col-03'

const router = { 
  path: '/wj03',
  name: 'wj03',
  component: wj03,
  children: [{
    path: '/wj06/index',
    components: {
      default: head,
      leftCol_01,
      leftCol_02,
      leftCol_03,
      midCol_01,
      midCol_02,
      rightCol_01,
      rightCol_02,
      rightCol_03
    }
  }]
}

export default router
