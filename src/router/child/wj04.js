import wj04 from '@/pages/wj04/index'

import head from '@/pages/wj04/header/header'

import leftCol_01 from '@/pages/wj04/left-col/col-01'
import leftCol_02 from '@/pages/wj04/left-col/col-02'
import leftCol_03 from '@/pages/wj04/left-col/col-03'
import midCol_01 from '@/pages/wj04/mid-col/col-01'
import midCol_02 from '@/pages/wj04/mid-col/col-02'
import midCol_03 from '@/pages/wj04/mid-col/col-03'
import midCol_04 from '@/pages/wj04/mid-col/col-04'
import midCol_05 from '@/pages/wj04/mid-col/col-05'
import rightCol_01 from '@/pages/wj04/right-col/col-01'
import rightCol_02 from '@/pages/wj04/right-col/col-02'
import rightCol_03 from '@/pages/wj04/right-col/col-03'
import rightCol_04 from '@/pages/wj04/right-col/col-04'

const router = {
    path: '/wj04',
    name: 'wj04',
    component: wj04,
    children: [{
        path: '/wj07/index',
        components: {
            default: head,
            leftCol_01,
            leftCol_02,
			leftCol_03,
            midCol_01,
            midCol_02,
            midCol_03,
            midCol_04,
            rightCol_01,
            rightCol_02,
			rightCol_03,
			rightCol_04,
            midCol_05
        }
    }]
}

export default router