import wj08 from '@/pages/wj08/index'
import head from '@/pages/wj07/header/header'
import bgimg from '@/pages/wj08/bodyall'
const router = {
    path: '/wj08',
    name: 'wj08',
    component: wj08,
    children: [{
        path: '/wj088/index',
        components: {
            default: head,
            bgimg
        }
    }]
}

export default router