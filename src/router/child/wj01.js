
import wj01 from '@/pages/wj01/index'

import head from '@/pages/wj01/header/header'
import posts from '@/pages/wj01/posts/posts'
import salary from '@/pages/wj01/salary/salary'
import resume from '@/pages/wj01/resume/resume'
import personnelStructure from '@/pages/wj01/personnelStructure/personnelStructure'
import signIn from '@/pages/wj01/signIn/signIn'
import personnelResource from '@/pages/wj01/personnelResource/personnelResource'
import wordCloud from '@/pages/wj01/wordCloud/wordCloud'
import zzQs from '@/pages/wj01/zzQs/zzQs'
import personnelPercent from '@/pages/wj01/personnelPercent/personnelPercent'
import recruitEnterprise from '@/pages/wj01/recruitEnterprise/recruitEnterprise'

const router = { 
  path: '/wj01',
  name: 'wj01',
  component: wj01,
  children:[{
    path: '/wj01/index',
    components: {
      default: head,
      posts,
      salary,
      resume,
      personnelStructure,
      signIn,
      personnelResource,
      wordCloud,
	  zzQs,
      personnelPercent,
      recruitEnterprise,
    }
  }]
}

export default router