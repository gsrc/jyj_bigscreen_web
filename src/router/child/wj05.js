
import wj05 from '@/pages/wj05/index'

import head from '@/pages/wj05/header/header'

import leftCol_01 from '@/pages/wj05/left-col/col-01'
import leftCol_02 from '@/pages/wj05/left-col/col-02'
import midCol_01 from '@/pages/wj05/mid-col/col-01'
import midCol_02 from '@/pages/wj05/mid-col/col-02'
import rightCol_01 from '@/pages/wj05/right-col/col-01'
import rightCol_02 from '@/pages/wj05/right-col/col-02'
import bottomCol_01 from '@/pages/wj05/bottom-col/col-01'
import bottomCol_02 from '@/pages/wj05/bottom-col/col-02'

const router = { 
  path: '/wj05',
  name: 'wj05',
  component: wj05,
  children: [{
    path: '/wj05/index',
    components: {
      default: head,
      leftCol_01,
      leftCol_02,
      midCol_01,
      midCol_02,
      rightCol_01,
      rightCol_02,
      bottomCol_01,
      bottomCol_02
    }
  }]
}

export default router
