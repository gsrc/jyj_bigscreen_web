import wj02 from '@/pages/wj02/index'

import head from '@/pages/wj02/header/header'

import leftCol_01 from '@/pages/wj02/left-col/col-01'
import leftCol_02 from '@/pages/wj02/left-col/col-02'
import midCol_01 from '@/pages/wj02/mid-col/col-01'
import midCol_02 from '@/pages/wj02/mid-col/col-02'
import midCol_03 from '@/pages/wj02/mid-col/col-03'
import rightCol_01 from '@/pages/wj02/right-col/col-01'

const router = {
  path: '/wj02',
  name: 'wj02',
  component: wj02,
  children:[{
    path: '/wj04/index',
    components: {
      default: head,
      leftCol_01,
      leftCol_02,
      midCol_01,
      midCol_02,
      midCol_03,
      rightCol_01
    }
  }]
}

export default router
