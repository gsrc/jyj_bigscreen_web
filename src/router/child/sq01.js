import sq01 from '@/pages/sq01/index'

import head from '@/pages/sq01/header/header'
import leftCol_01 from '@/pages/sq01/left-col/col-01'
import leftCol_02 from '@/pages/sq01/left-col/col-02'
import midCol_01 from '@/pages/sq01/mid-col/col-01'
import midCol_02 from '@/pages/sq01/mid-col/col-02'
import midCol_03 from '@/pages/sq01/mid-col/col-03'
import rightCol_01 from '@/pages/sq01/right-col/col-01'
import rightCol_02 from '@/pages/sq01/right-col/col-02'
import rightCol_03 from '@/pages/sq01/right-col/col-03'

const router = {
    path: '/sq01',
    name: 'sq01',
    component: sq01,
    children: [{
        path: '/sq01/index',
        meta: {
            title: '智慧社区大屏展示'
        },
        components: {
            default: head,
            leftCol_01,
            leftCol_02,
            midCol_01,
            midCol_02,
            midCol_03,
            rightCol_01,
            rightCol_02,
            rightCol_03
        }
    }],
	meta: {
		title: "万盛社区监控大屏"
	}
}

export default router