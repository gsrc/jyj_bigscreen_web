import wj06 from '@/pages/wj06/index'

import head from '@/pages/wj06/header/header'
import leftCol_01 from '@/pages/wj06/left-col/col-01'
import rightCol_01 from '@/pages/wj06/right-col/col-01'
import midCol_01 from '@/pages/wj06/mid-col/col-01'
import midCol_02 from '@/pages/wj06/mid-col/col-02'
import midCol_03 from '@/pages/wj06/mid-col/col-03'
import midCol_04 from '@/pages/wj06/mid-col/col-04'

const router = {
    path: '/wj06',
    name: 'wj06',
    component: wj06,
    children: [{
        path: '/wj03/index',
        components: {
            default: head,
            leftCol_01,
            rightCol_01,
            midCol_01,
            midCol_02,
            midCol_03,
            midCol_04
        }
    }]
}

export default router