import wj07 from '@/pages/wj07/index'

import head from '@/pages/wj07/header/header'
import leftCol_01 from '@/pages/wj07/left-col/col-01'
import leftCol_02 from '@/pages/wj07/left-col/col-02'
import midCol_01 from '@/pages/wj07/mid-col/col-01'
import midCol_02 from '@/pages/wj07/mid-col/col-02'
import rightCol_01 from '@/pages/wj07/right-col/col-01'
import rightCol_02 from '@/pages/wj07/right-col/col-02'

const router = {
    path: '/wj07',
    name: 'wj07',
    component: wj07,
    children: [{
        path: '/wj02/index',
        components: {
            default: head,
            leftCol_01,
            leftCol_02,
            midCol_01,
            midCol_02,
            rightCol_01,
            rightCol_02
        }
    }]
}

export default router