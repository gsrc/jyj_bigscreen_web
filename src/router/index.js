import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

let file = require.context('./child', true, /\.js$/)

let routerArr = [];
file.keys().forEach(key => {
    routerArr = [...routerArr, file(key).default]
})

console.log(routerArr);

const router = new Router({
    routes: [{
            path: '/',
            name: 'root',
            redirect: '/wj01/index'
        },
        ...routerArr
    ]
})
router.beforeEach((to, from, next) => {
    console.log(to)
    if (to.path == '/sq01/index') {
        window.document.title = to.meta.title
    }

    next()
})
export default router