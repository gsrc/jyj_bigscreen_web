import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    // 组件中想访问 使用this.$store.state.count
    state: {
        token: null,
        date: '2020-01-17',
        params: {
            month: '2020-01-17'
        },
        sarlary: null
    },
    actions: {
        // incrementHandle(ctx, num) {
        // }
    },
    // 通过mutations修改state数据，调用this.$store.commit("方法名")
    mutations: {
        UPDATE_DATE(state, payload) {
            state.date = payload;
        },
        UPDATE_PARAMS(state, payload) {
            state.params = payload;
        },
        UPDATE_SARLY(state, payload) {
            state.sarlary = payload
        },
        UPDATE_SA(state) {
            state.sarlary = null
        }
    },
    getters: {}
})