import HttpRequest from '@/utils/axios'

// dev: https://www.easy-mock.com/mock/5cee97db65dd5c3a44db25e4/ioc/
// export const baseUrl = process.env.BUILD_ENV === 'prod'
//   ? 'http://dataapi.jywj12333.com:90/api'
//   : 'http://dataapi.jywj12333.com:90/api'
export const baseUrl = process.env.BUILD_ENV === 'prod'
  ? 'http://linux.jywj12333.com/api'
  : 'http://linux.jywj12333.com/api'


const axios = new HttpRequest(baseUrl)
export default axios
