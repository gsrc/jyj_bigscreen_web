let express = require("express");
let proxy = require("http-proxy-middleware");

let app = express();

app.use("/shopactivity/", proxy({ target: "http://emcs.quanyou.com.cn/shopactivity/", changeOrigin: true }));

app.listen(8081);