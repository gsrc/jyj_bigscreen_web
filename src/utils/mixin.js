import Cookies from 'js-cookie'
import Vue from 'vue'
import Vuex from 'vuex'
import moment from 'moment'

Vue.use(Vuex)
let JSESSIONID = 'JSESSIONID'

const pubJs = {
  data() {
    return {
      swiperOption: {
        direction : 'vertical',
        slidesPerView: Math.floor(this.height/80),
        spaceBetween: 0,
        autoplay: true,
        speed: 300,
        loop: true
      },
      swiperOption2: {
        direction : 'vertical',
        slidesPerView: Math.floor(this.height/55),
        spaceBetween: 0,
        autoplay: true,
        speed: 300,
        loop: true
      }
    }
  },
  computed: {
    // swiper() {
    //   return this.$refs.mySwiper.swiper
    // }
  },
  methods: {
    // 获取本地Session
    GetSession() {
      return Cookies.get(JSESSIONID)
    },
    // 本地储存Session
    SetSession(session) {
      return Cookies.set(JSESSIONID, session)
    },
    // 删除本地Session
    RemoveSession() {
      this.RemoveUserName()
      return Cookies.remove(JSESSIONID)
    },
    //格式化日期
    formatDate(date,str) {
      if(str === 'quarter'){
        return moment(date).quarter();
      }else{
        return moment(date).format(str);
      }
    },
    data(num,count) {
      let arr = [];
      for(let i = 0; i< count; i++) {
        arr = [...arr,Number((Math.random()*num).toFixed(0))]
      }
      return arr;
    }
  }
};

export default pubJs
