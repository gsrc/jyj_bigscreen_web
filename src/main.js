// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'normalize.css'
import 'element-ui/lib/theme-chalk/index.css'
import './main.styl'
import store from './store'
import pubJs from './utils/mixin.js'
import 'babel-polyfill'
import 'es6-promise/auto'
import './utils/filter' // global filter
import 'styles/iconfont.css'
import './assets/css/border.css'
import './assets/css/sq-border.css'
import axios from '@/utils/api.request'


Vue.use(ElementUI)
Vue.mixin(pubJs)

//挂载图表
import echatrs from 'echarts';
require('echarts-wordcloud')
Vue.prototype.$echarts = echatrs;

//公用组件
import rankListItem from "@/pages/wj02/components/rank-list-item";
Vue.component('rank-list-item', rankListItem)

//swiper
import VueAwesomeSwiper from 'vue-awesome-swiper'

// require styles
import 'swiper/dist/css/swiper.css'

//加载动画
Vue.component('animae-jz', {
    template: `<div class="container_jz">
            <div class="circle_jz"></div>
            <div class="circle_jz"></div>
            <div class="circle_jz"></div>
            </div>`
})


Vue.use(VueAwesomeSwiper, /* { default global options } */ )

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
})

Vue.prototype.mapIndex = '';

var selfUrl = window.location.href;

function getNowPage(){
	var channel = '';

	if (window.screen.width > 2000){
		channel = 'bgs';
	}

	var result = axios.request({
	    url: 'http://bigscreen.jywj12333.com:90/getPage?channel=' + channel,
	    method: 'get'
	}).then((response)=>{
		
		if (response.data !== 0){
			console.log(response);
			// var info = JSON.parse(response.data);
			var info = response.data;
			var url = '/#/wj'+info.pageIndex+'/index?range='+info.selectIndex + '&map=' + info.mapIndex;
			var pageRoute = '/#/wj'+info.pageIndex+'/index';
			Vue.prototype.mapIndex = info.mapIndex;
		    console.log('get mapIndex1: ' + info.mapIndex);
			console.log('get mapIndex2: ' + Vue.prototype.mapIndex);
			if (selfUrl.indexOf(url) != -1){
				
			}else{
				if (window.location.href.indexOf(pageRoute) != -1){
					window.location.href = url;
					if (!info.mapIndex){
						window.location.reload();
					}
				}else{
					window.location.href = url;
				}
			}
		}
    });
}

setInterval(function(){
	getNowPage();
}, 1000);

function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}